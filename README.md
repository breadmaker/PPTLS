# Piedra-Papel-Tijera-Lagarto-Spock

Antiguamente un 404, para probar diferentes ideas para un desarrollo más rápido,
ahora se conserva como una aplicación independiente responsiva para ser accedida
desde cualquier dispositivo.

Utiliza [Stylus](https://learnboost.github.io/stylus/) para compilar el CSS, y
[NPM](https://www.npmjs.com/) para las labores de construcción (Si, NPM,
[sin Grunt o Gulp](http://blog.keithcirkel.co.uk/how-to-use-npm-as-a-build-tool/)).

## Objetivo del proyecto

Como podrán darse cuenta, no tendría sentido armar este repositorio, si no fuera
a ser un aporte para ustedes. Lo que estoy haciendo es hacer una versión en
HTML5 del juego
[Piedra-Papel-Tijera-Lagarto-Spock](https://en.wikipedia.org/wiki/Rock-paper-scissors#Additional_weapons),
(hecho popular en el octavo capítulo de la segunda temporada de
[Big Bang Theory](https://es.wikipedia.org/wiki/Anexo:Segunda_temporada_de_The_Big_Bang_Theory),
"La expansión Lagarto-Spock"), la que por supuesto se podrá integrar fácilmente
en cualquier parte de un sitio web e funcionar desde cualquier dispositivo.

## Respecto de la construcción

Como indiqué más arriba, estoy usando NPM para construir, sin usar Grunt, Gulp u
otras herramientas semejantes, gracias al objeto `scripts` presente en el
archivo
[`package.json`](https://gitlab.com/radiognu/PPTLS/blob/master/package.json).

Si quieres hacer modificaciones, deberás realizar los siguientes pasos. Lo
primero es obtener las dependencias:

```bash
$ cd /ruta/hacia/PPTLS
$ npm install
```

Una vez modificado, es necesario construir, eso se hace con la siguiente
instrucción:

```bash
$ npm run build
```

O puedes construir cada vez que se modifique un archivo con la instrucción:

```bash
$ npm run build:watch
```
